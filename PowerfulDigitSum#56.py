#Powerful digit sum Problem 56
#!/usr/bin/python3
def main():
    res=0;
    for a in range(100):
        for b in range(100):
            s=0;
            n=a**b;
            n=str(n);
            if res<digitalSum(n):
                res=digitalSum(n);
    print(res);

def digitalSum(n):
    s=0
    for i in n:
        s=s+int(i);
    return s;

if __name__ == '__main__':
    main();
