#Double-base palindromes #36
#!/usr/bin/python3
p="";q="";w=0;
for i in range(1,1000000):
    s=str(i);
    for j in range(1, len(s)+1):
        p=p+s[-j];
    if i==int(p):
        b=str(bin(i))[2:];
        for j in range(1, len(b)+1):
            q=q+b[-j];
        if b==q:
            w=w+i;
            print(str(i)+" - "+b+" - "+q);
        q="";
    p="";
print(w);
