#! /usr/bin/python3
collatzPair={};
for i in range(2,1000000+1):
    n=str(i);
    count=0;
    while i>1:
        if i%2==0:
            i=i//2;
        else:
            i=(3*i)+1;
        count=count+1;
    collatzPair[n]=count;
l=sorted(collatzPair.values(), reverse=True);
for k,v in collatzPair.items():
    if l[0]==v:
        print(k);
