#!/usr/bin/python3
#What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
from itertools import permutations;
l=list(permutations(range(0,10)))
print(l[1000000-1]);
