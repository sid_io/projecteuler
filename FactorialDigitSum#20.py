#!/usr/bin/python3
def factorial(n):
    if n<=1:
        return n;
    else:
        return n*factorial(n-1);

def digitSum(fctrl):
    n=str(fctrl)
    sum=0;
    for i in range(len(n)):
        sum=sum+int(n[i]);
    return sum;

def main():
    n=int(input("Enter a number: "));
    fctrl=factorial(n);
    print(digitSum(fctrl));

if __name__ == '__main__':
    main();
