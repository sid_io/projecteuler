#Lychrel numbers Problem 55
#!/usr/bin/python3
def adder(n):
    m=int(str(n)[::-1]);
    sum=n+m;
    return sum;

def main():
    res=0;
    for i in range(1, 10000):
        count=0;
        sum=i;
        while count<=50:
            sum=adder(sum);
            count=count+1;
            if len(str(sum))>1:
                if sum==int(str(sum)[::-1]):
                    break;
        if count>=50:
            res=res+1;
    print(res);
if __name__ == '__main__':
    main();
